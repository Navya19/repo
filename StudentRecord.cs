﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Set34
{
    public class StudentRecord
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter the size");
            int size = int.Parse(Console.ReadLine());
            Student[] s = new Student[size];

            UpdateCode();

            MergeCode();

            for (int i = 0; i < size; i++)
            {
                Console.WriteLine("enter the details of " + (i + 1) + "student");
                Console.WriteLine("enter the name");
                string name = Console.ReadLine();
                Console.WriteLine("enter the student id");
                int studentId = int.Parse(Console.ReadLine());
                Console.WriteLine("enter branch");
                string branch = Console.ReadLine();

                s[i] = new Student(studentId, name, branch);
            }
            //GetStudentIdAsc(s);
            //GetStudentNameInSorted(s);

            Console.WriteLine("Enter the choice: ");
            int choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    GetStudentNameInSorted(s);
                    break;
                case 2:
                    GetStudentIdAsc(s);
                    break;
            }
        }

        public static void UpdatingCode()
        {
            Console.Write("updated");
        }
        private static void GetStudentIdAsc(Student[] s)
        {
            Student temp = new Student();
            for (int j = 0; j < s.Length - 1; j++)
            {
                for (int i = j + 1; i < s.Length; i++)
                {
                    if (s[j].getStudentId() > s[i].getStudentId())
                    {
                        temp = s[j];
                        s[j] = s[i];
                        s[i] = temp;
                    }
                }
            }
            for (int i = 0; i < s.Length; i++)
            {
                Console.WriteLine(s[i].getStudentId() + " " + s[i].getName());
            }

        }

        private static void GetStudentNameInSorted(Student[] s)
        {

            Student temp = new Student();
            for (int j = 0; j < s.Length - 1; j++)
            {
                for (int i = j + 1; i < s.Length; i++)
                {
                    if (s[j].getBranch().CompareTo(s[i].getBranch()) > 0)
                    {
                        temp = s[j];
                        s[j] = s[i];
                        s[i] = temp;
                    }
                }
            }
            for (int i = 0; i < s.Length; i++)
            {
                Console.WriteLine(s[i].getBranch() + " " + s[i].getName());
                Console.ReadLine();
            }
        }
       
    }
}