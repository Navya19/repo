﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Set34
{
    class Student
    {
        private int studentId;
		private string name;
		private string branch; 


		public String getName()
		{
			return name;
		}
		public void setName(String name)
		{
			this.name = name;
		}
		public String getBranch()
		{
			return branch;
		}
		public void setBranch(String branch)
		{
			this.branch = branch;
		}
        public int getStudentId()
        {
            return studentId;
        }
        public void setStudentId(int studentid)
        {
            this.studentId = studentid;
        }

        public Student()
        {

        }

        public Student(int studentId,string name,string branch)
        {
            this.studentId = studentId;
            this.name = name;
            this.branch = branch;
        }
    }
}
